// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef DIAGNOSTICS_WILCO_DTC_SUPPORTD_VPD_CONSTANTS_H_
#define DIAGNOSTICS_WILCO_DTC_SUPPORTD_VPD_CONSTANTS_H_

namespace diagnostics {

extern const char kVpdFieldSerialNumberFilePath[];

}  // namespace diagnostics

#endif  // DIAGNOSTICS_WILCO_DTC_SUPPORTD_VPD_CONSTANTS_H_
