%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='platforms')

  %# --------------------------------------------------------------------------
  %# Platforms
  %if not tpl_vars.get('platforms'):
    <h3><u>No test platforms found.</u></h3>
  %else:
    <div id="divPlatforms" class="lefted">
      <table class="alternate_background">
        <tbody>
          <tr>
            <th class="headeritem centered">
              Platform
            </th>
            <th class="headeritem centered">
              Dashboard
            </th>
            <th class="headeritem centered">
              Platform view
            </th>
          </tr>
          %for _platform, _bvt_link, _platform_link in tpl_vars['platforms']:
            <tr>
              <td class="centered">
                {{ _platform }}
              </td>
              <td class="centered">
                {{! _bvt_link }}
              </td>
              <td class="centered">
                {{! _platform_link }}
              </td>
            </tr>
          %end
        </tbody>
      </table>
      <div class="headeritem lefted">
        {{ len(tpl_vars['platforms']) }} total platforms
      </div>
    </div>
  %end
%end

%rebase('master.tpl', title='platforms', query_string=query_string, body_block=body_block)
