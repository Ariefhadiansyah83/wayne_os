# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "jamescook@chromium.org, mustash-team@google.com"
NAME = "desktopui_SingleProcessMashLogin"
PURPOSE = "Verify chrome --enable-features=SingleProcessMash OOBE and login."
CRITERIA = "Fails if chrome --enable-features=SingleProcessMash does not start."
# Run in the chrome-tot-informational builders. Release per-build testing
# is handled by Tast test ui.SingleProcessMashLogin.
ATTRIBUTES = "suite:chrome-informational"
JOB_RETRIES = 0
TIME = "SHORT"
TEST_CATEGORY = "Functional"
TEST_CLASS = "desktopui"
TEST_TYPE = "client"
BUG_TEMPLATE = {
  'components': ['Internals>Services>WindowService'],
  'labels': ['Proj-Mash-SingleProcess'],
  'cc': ['mustash-autotest-bugs@google.com'],
}

DOC = """
chrome --enable-features=SingleProcessMash has significant differences in its
initialization vs. both regular chrome on devices and chrome on Linux
desktop. This test verifies chrome can start up and log in. If it fails, please
contact mustash-team@google.com.
"""

job.run_test('desktopui_SingleProcessMashLogin')
