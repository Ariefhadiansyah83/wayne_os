# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from fuzzy_check import FuzzyCheck
from validators import *

# originally generated gestures:
# FlingStop
#   Motion d=99.85 x=26.09 y=96 r=0.04698 s=63
#   FlingStop
#   Scroll d=55.06 x=2.027 y=54.22 r=0.1572 s=195.3
#   Fling d=73.76 x=52.16 y=52.15 r=0 s=7596

def Validate(raw, events, gestures):
  fuzzy = FuzzyCheck()
  fuzzy.expected = [
    ScrollValidator(">= 100"),
  ]
  fuzzy.unexpected = [
    FlingValidator(),
    FlingStopValidator("<10"),
  ]
  return fuzzy.Check(gestures)
