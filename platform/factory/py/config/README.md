ChromeOS Factory Software Build Configuration
=============================================

This directory contains *Build configuration* JSON config and schema files and
will be visited by module `cros.factory.utils.config_utils`.

To add files, create the right JSON (config and schema) files in board overlay
 `factory-board/files/py/config/` and install into system.

