# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Start local devserver."
author        "chromium-os-dev@chromium.org"

start on (started moblab-apache-init and
          stopped moblab-external-storage-init RESULT=ok and
          stopped moblab-internetcheck RESULT=ok)

# STATIC_DIR should be the same as in moblab-external-storage-init.conf
# It's set here to allow devserver to be restarted without OS reboot.
env STATIC_DIR=/mnt/moblab/static
env LOG_DIR=/var/log/devserver
env CROS_CACHEDIR=/mnt/moblab/cros_cache

normal exit 0

pre-start script
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e
  logger -t "${UPSTART_JOB}" "Pre Start Starting."
  mkdir -p "${LOG_DIR}"
  chown -R moblab:moblab "${LOG_DIR}"
  mkdir -p "${CROS_CACHEDIR}"
  chown -R moblab:moblab "${CROS_CACHEDIR}"
  for script in /usr/lib/devserver/dut-scripts/*; do
    ln -sf "${script}" "${STATIC_DIR}"
  done
end script

exec sudo -u moblab CROS_CACHEDIR="${CROS_CACHEDIR}" \
  /usr/lib/devserver/devserver.py --production \
  --static_dir="${STATIC_DIR}" --logfile "${LOG_DIR}/server.log" \
  --android_build_credential="/home/moblab/.launch_control_key" \
  >>"${LOG_DIR}/console.log" 2>&1
