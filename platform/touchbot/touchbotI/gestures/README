This is a set of robot control programs.  They vary in complexity, but
all are generally useful in programming the robot.  They are:


	run_program		When run by itself this "gesture" simply tells
				the robot to run whatever program is currently
				in memory.  Otherwise it can be imported to do
				all the setup for programming a new gesture.
				Read basic.py for an example of how to do this.

	basic			Intended mostly as an example of how to
				program the robot, this is a very simple
				gesture that simple traverses the perimeter
				of the touchpad, but can be useful in
				calibrating the robot for a new device.

	line			Make the robot run along a straight line
				specified via command line arguments on the
				touchpad at a similarly specified speed.

	scroll			Does a two-finger "scroll" gesture.  It touches
				the pad at the top and  moves down.  If passed
				the command line argument "fling" it will lift
				off smoothly.  If not, it will pause before
				lifting off the pad.

	parameter_reset		This resets the robot parameters; it
				should not be necessary to zero memory,
				but if you end up needing to, this puts
				everything back to where it should be.

	diagonals		Make the robot move in a tight, diagonal criss-
				cross pattern over the whole touchpad.  This
				allows us to see linearity errors in the sensor
				and compensate for them in the future.

	goto_origin		Move the robot finger to its origin
				position.  This tends to be useful.

	point_picker		Pick a point; this provides a graphical
				representation of the X, Y, and Z location
				of the finger.  'o' returns to origin, 'q'
				quits, '1', '2', '3', and '4' provide
				smaller motion resolution, cursor left and
				right pick an axis, and cursor up moves
				along that axis toward the origin, while
				cursor down moves away.  I typically hit
				return after the robot is fully done moving,
				which updates the current coordinate display.

				This is almost the only reasonable way to
				get a registration for the touch area
				bounding box with any degree of accuracy,
				since we don't have a teaching pendant.

End Of Document.
